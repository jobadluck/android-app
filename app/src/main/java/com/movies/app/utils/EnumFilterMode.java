package com.movies.app.utils;

import com.movies.app.R;

import java.util.HashMap;

public enum EnumFilterMode {
    // Sub Server NE types
    ALL(0, "all", R.color.colorAll),
    PROD_FILTER(1,"prod id", R.color.colorProd),
    DISNEY_CLASSIC_ID(2,"disney classic id", R.color.colorDisneyClassic),
    ANIMATED_CLASSIC_ID(3,"animated classic id", R.color.colorAnimClassic);

    private final int mode;
    private final String type;
    private final int color;

    private static final HashMap<Integer, EnumFilterMode> map = new HashMap();
    static {
        for(EnumFilterMode t : EnumFilterMode.values())
            map.put(t.mode, t);
    }

    private EnumFilterMode(int mode, String type, int color) {
        this.mode = mode;
        this.type = type;
        this.color = color;
    }

    public int mode(){
        return mode;
    }

    public String type(){
        return type;
    }

    public int color() {
        return color;
    }

    public static EnumFilterMode getByValue(int mode){
        return map.get(mode);
    }
}
