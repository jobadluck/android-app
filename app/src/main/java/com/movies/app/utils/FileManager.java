package com.movies.app.utils;

import android.content.Context;

import com.movies.app.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 * Class providing methods to load files data
 */
public class FileManager {
    private final static String SAVE_NAME = "save";
    private final static String SAVE_FILE = SAVE_NAME + ".csv";
    private final static String LIST_NAME = "movies.csv";
    public final static String NO_DATA = "No data";

    public static ArrayList<String> getSaveFile(Context context, int size){
        InputStream save = saveFileToInputStream(context, size);
        ArrayList<String> saveToStream = parseInputStreamToArray(save);
        if(saveToStream.size() != size){
            // Reinit save data array if its size different from movie array size
            return parseInputStreamToArray(initSaveFile(context, size));
        } else {
            return saveToStream;
        }
    }

    public static ArrayList<String> getMoviesDataFile(Context context){
        InputStream list = context.getResources().openRawResource(R.raw.movies);
        return parseInputStreamToArray(list);
    }

    /**
     * Method to update data written in save file
     */
    public static void updateSaveFile(Context context, ArrayList<Movie> data){
        System.out.println();
        try {
            FileOutputStream test = new FileOutputStream(new File(context.getFilesDir(), SAVE_FILE));
            StringBuilder body = new StringBuilder();
            for(int m = 0; m < data.size(); ++m){
                body.append(data.get(m).getPresent()? "1\n" : "0\n");
            }
            test.write(body.toString().getBytes());
            test.close();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    /**
     * Method to collect parsed data, even null ones
     */
    public static String getData(String data){
        if(data == null || data.equals("--")){
            return NO_DATA;
        } else {
            return data;
        }
    }

    /**
     * Method to get save file data
     */
    private static InputStream saveFileToInputStream(Context context, int size) {
        System.out.println();
        try {
            FileInputStream test = context.openFileInput(SAVE_FILE);
            return test;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Method returning data file to an array list
     */
    private static ArrayList<String> parseInputStreamToArray(InputStream file){
        ArrayList<String> data = new ArrayList<>();
        if(file != null) {
            Scanner s = new Scanner(file).useDelimiter("\n");
            while (s.hasNext()) {
                data.add(normalize(s.next()));
            }
        }
        return data;
    }

    private static String normalize(String word) {
        return word.replaceAll("\"","");
    }

    /**
     * Method to (re)init save.csv file containing presence values for each movies
     */
    private static FileInputStream initSaveFile(Context context, int size) {
        try {
            FileOutputStream test = new FileOutputStream(new File(context.getFilesDir(), SAVE_FILE));
            String value = "0\n";
            String body = String.join("", Collections.nCopies(size, value));
            test.write(body.getBytes());
            test.close();
            return context.openFileInput(SAVE_FILE);
        } catch (IOException e1) {
            e1.printStackTrace();
            return null;
        }
    }
}
