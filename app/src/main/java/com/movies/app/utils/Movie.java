package com.movies.app.utils;

public class Movie {
    private String _title;
    private String _en_title;
    private String _prod_id;
    private String _disney_classic_id;
    private String _animated_classic_id;
    private String _date;
    private boolean _present;

    public Movie(){
        _title = "";
        _en_title = "";
        _prod_id = "";
        _disney_classic_id = "";
        _animated_classic_id = "";
        _date = "";
        _present = false;
    }

    public Movie(Movie m){
        _title = m.getTitle();
        _en_title = m.getEnglishTitle();
        _prod_id = m.getProdId();
        _disney_classic_id = m.getDisneyClassicId();
        _animated_classic_id = m.getAnimatedClassicId();
        _date = m.getReleaseDate();
        _present = m.getPresent();
    }

    public Movie(String title, String englishTitle, String prodId, String disneyClassicId,
                 String animatedClassicId, String releaseDate, boolean present){
        _title = title;
        _en_title = englishTitle;
        _prod_id = prodId;
        _disney_classic_id = disneyClassicId;
        _animated_classic_id = animatedClassicId;
        _date = releaseDate;
        _present = present;
    }

    public String getTitle() {
        return _title;
    }

    public String getEnglishTitle() {
        return _en_title;
    }

    public String getProdId() {
        return _prod_id;
    }

    public String getDisneyClassicId() {
        return _disney_classic_id;
    }

    public String getAnimatedClassicId() {
        return _animated_classic_id;
    }

    public String getReleaseDate() {
        return _date;
    }

    @Override
    public String toString() {
        String englishTitle = getEnglishTitle().equals(getTitle())
                || getEnglishTitle().equals(FileManager.NO_DATA)?
                "" : " (" + getEnglishTitle() + ")";
        return  "Titre: " + getTitle() + englishTitle + "\n"
                + "Année de sortie: " + getReleaseDate() + "\n"
                + "Prod ID: " + getProdId() + "\n"
                + "Disney Classic ID: " + getDisneyClassicId() + "\n"
                + "Animated Classic ID: " + getAnimatedClassicId();
    }

    public boolean getPresent(){
        return _present;
    }

    public boolean checkMoviebyFilter(int filter){
        switch(filter){
            case 1:
                return !getProdId().equals(FileManager.NO_DATA);
            case 2:
                return !getDisneyClassicId().equals(FileManager.NO_DATA);
            case 3:
                return !getAnimatedClassicId().equals(FileManager.NO_DATA);
            default:
                return true;
        }
    }

    public void setPresent(boolean isPresent){
        _present = isPresent;
    }
}
