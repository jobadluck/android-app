package com.movies.app.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class MovieListUtils {

    public static Logger LOGGER = Logger.getLogger(MovieListUtils.class.getName());

    public final static int ALL = 0;
    public final static int PROD_TYPE = 1;
    public final static int DISN_TYPE = 2;
    public final static int ANIM_TYPE = 3;

    /**
     * Method to search for movies containing @name string
     * Return the whole list if search is empty
     * TODO: add later button to reset search instead of empty search to get whole list
     */
    public static ArrayList<Movie> filterByTitle(String name){
        ArrayList<Movie> searchResult = new ArrayList<>();
        if(!name.isEmpty()) {
            for (Movie m : SaveManager.getDataList()) {
                if ((m.getTitle()).toLowerCase().contains(name.toLowerCase())) {
                    searchResult.add(m);
                }
            }
            return searchResult;
        }
        return SaveManager.getDataList();
    }

    /**
     * Method to filter movies by their kind
     * If a movie has its ID corresponding to the type searched null, it is not returned
     */
    public static ArrayList<Movie> filterByType(int type) {
        ArrayList<Movie> searchResult = new ArrayList<>();
        if(type > 0) {
            for (Movie m : SaveManager.getDataList()) {
                if ((type == MovieListUtils.PROD_TYPE && !m.getProdId().equals(FileManager.NO_DATA))
                        || (type == MovieListUtils.DISN_TYPE && !m.getDisneyClassicId().equals(FileManager.NO_DATA))
                        || (type == MovieListUtils.ANIM_TYPE && !m.getAnimatedClassicId().equals(FileManager.NO_DATA))) {
                    searchResult.add(m);
                }
            }
            return searchResult;
        }
        return SaveManager.getDataList();
    }

    public static ArrayList<Movie> createList(ArrayList<String> mData, ArrayList<String> mSave) {
        ArrayList<Movie> init = new ArrayList<>();
        if(mData.size() == mSave.size()){
            for(int m=0; m<mData.size(); ++m){
                Matcher data = Pattern.compile("(.*?),(.*?),(.*?)(.*?),(.*?),(.*?)").matcher(mData.get(m));
                // No de production /	No de « Classique Disney » / No de « Classique d'animation »
                if(data.matches()) {
                    try{
                        ArrayList<String> movieData = new ArrayList<>(
                                Arrays.asList(mData.get(m).split(",")));
                        String prod_id = FileManager.getData(movieData.get(0));
                        String disney_classic_id = FileManager.getData( movieData.get(1));
                        String animated_classic_id = FileManager.getData(movieData.get(2));
                        String title = FileManager.getData(movieData.get(3));
                        String en_title = FileManager.getData(movieData.get(4));
                        String date = FileManager.getData(movieData.get(5));
                        init.add(new Movie(title, en_title, prod_id, disney_classic_id, animated_classic_id, date,
                                Integer.valueOf(mSave.get(m)) == 0 ? false : true));
                    } catch(RuntimeException e){
                        LOGGER.log(Level.SEVERE, "Error in Movie data parsing: " + mData.get(m), e);
                    }
                } else {
                    LOGGER.log(Level.SEVERE, "Error in Movie data parsing: " + mData.get(m));
                }
            }
        }
        return init;
    }

    /**
     * Search in list movie named @title
     */
    public static Movie getByTitle(ArrayList<Movie> list, String title) {
        for(Movie m: list){
            if(m.getTitle().equals(title)){
                return m;
            }
        }
        return null;
    }

    /**
     * Method to generate current selected movies list in String format
     */
    public static String generateCurrentListString(int filterMode) {
        return SaveManager.getDataList().stream()
                .filter(movie -> movie.getPresent() && movie.checkMoviebyFilter(filterMode))
                .map(Movie::getTitle)
                .collect(Collectors.joining( "\n" ));
    }
}
