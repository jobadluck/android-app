package com.movies.app.utils;

import java.util.ArrayList;

/**
 * Singleton to manage save
 */
public class SaveManager {
    private static SaveManager INSTANCE;
    private static ArrayList<Movie> DATA_LIST = new ArrayList<>();

    private SaveManager() {}

    public static SaveManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new SaveManager();
        }
        return INSTANCE;
    }

    /**
     * Update movies presence:
     *  0 => false, not present
     *  1 => true
     */
    public static void updateMoviesStatus(ArrayList<Movie> movieList){
        for(Movie m: movieList){
            Movie mToUpdate = MovieListUtils.getByTitle(DATA_LIST, m.getTitle());
            if(mToUpdate == null) {
                DATA_LIST.add(new Movie(m));
            } else {
                mToUpdate.setPresent(m.getPresent());
            }
        }
    }

    /**
     * Update a specific movie presence
     */
    public static void updateMoviesStatus(Movie m) {
        Movie mToUpdate = MovieListUtils.getByTitle(DATA_LIST, m.getTitle());
        if (mToUpdate == null) {
            DATA_LIST.add(new Movie(m));
        } else {
            mToUpdate.setPresent(m.getPresent());
        }
    }

    public static ArrayList<Movie> getDataList(){
        return DATA_LIST;
    }

}
