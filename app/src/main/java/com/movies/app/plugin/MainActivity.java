package com.movies.app.plugin;

import android.app.SearchManager;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.movies.app.R;
import com.movies.app.utils.EnumFilterMode;
import com.movies.app.utils.FileManager;
import com.movies.app.utils.Movie;
import com.movies.app.utils.MovieListUtils;
import com.movies.app.utils.SaveManager;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements RecyclerViewAdapter.ItemClickListener, SearchView.OnQueryTextListener{

    public static final String EXTRA_MESSAGE = "com.movies.app.MESSAGE";
    private RecyclerViewAdapter mAdapter;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private Toolbar mTopToolbar;
    private Toast toast;
    private EnumFilterMode currentMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ArrayList<String> mData = FileManager.getMoviesDataFile(this);
        ArrayList<String> mSave = FileManager.getSaveFile(this, mData.size());
        SaveManager.getInstance().updateMoviesStatus(MovieListUtils.createList(mData,mSave));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.list);

        // use a linear layout manager
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        mTopToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mTopToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        mAdapter = new RecyclerViewAdapter(this, SaveManager.getDataList());
        recyclerView.setAdapter(mAdapter);
        currentMode = EnumFilterMode.getByValue(0);
    }

    public void filterByTitle(String search){
        ArrayList<Movie> moviesSearch = MovieListUtils.filterByTitle(search);
        mAdapter.clear();
        mAdapter = new RecyclerViewAdapter(this, moviesSearch);
        recyclerView.setAdapter(mAdapter);
    }

    public void filterByType(int type){
        ArrayList<Movie> moviesSearch = MovieListUtils.filterByType(type);
        mAdapter.clear();
        mAdapter = new RecyclerViewAdapter(this, moviesSearch);
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onItemClick(View view, int position) {
        // TODO: may be used to open another page with selected movie informations
        if(mAdapter.getItemCount() > 0) {
            Movie movieClicked = mAdapter.getItem(position);
            String popup = movieClicked.toString();
            if (toast != null) {
                toast.cancel();
            }
            toast = Toast.makeText(this, popup, Toast.LENGTH_LONG);
            toast.show();
        }
    }

    @Override
    public boolean onItemLongClick(View view, int position) {
        // TODO: may be used to show options on a specific movie
//        if(mAdapter.getItemCount() > 0 && mAdapter.getItem(position) != null) {
//            mAdapter.removeItem(position);
//            Toast.makeText(this, "Remove " + (position+1) + "th element",
//                    Toast.LENGTH_SHORT).show();
//        } else {
//            Toast.makeText(this, "Empty list", Toast.LENGTH_SHORT).show();
//        }
        return true;
    }

    @Override
    public void onItemCheckChange(View view, int position){
//        if(mAdapter.getItemCount() > 0) {
//            Toast.makeText(this, "You checked " + mAdapter.getItemTitle(position) + " on row number " + position
//                    + " (over " + (mAdapter.getItemCount()+1) + " item(s))", Toast.LENGTH_SHORT).show();
//        } else {
//            Toast.makeText(this, "Empty list", Toast.LENGTH_SHORT).show();
//        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);
        getMenuInflater().inflate(R.menu.menu_list, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);

        SearchManager searchManager = (SearchManager) getSystemService(this.SEARCH_SERVICE);

        SearchView searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(MainActivity.this.getComponentName()));
        }
        searchView.setOnQueryTextListener(this);
        searchView.setIconified(false);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO: action on menu click
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            generateListClick();
//            Toast.makeText(MainActivity.this, "List Option clicked", Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        filterByTitle(newText);
        return true;
    }

    // TODO: Take in account current title search when appling a filter

    // TODO: During filter, background transition between pictures instead of simple colors (see below)
//    TransitionDrawable td = new TransitionDrawable( new Drawable[] {
//            getResources().getDrawable(mImages[x]),
//            getResources().getDrawable(mImages[y])
//    });
//    imageView.setImageDrawable(td);
//    td.startTransition(1000);
//    td.reverseTransition(1000);

    public void filterAll(View view) {
        applyFilter(0);
    }
    public void filterProd(View view) {
        applyFilter(1);
    }
    public void filterDisneyClassic(View view) {
        applyFilter(2);
    }
    public void filterAnimClassic(View view) {
        applyFilter(3);
    }

    private void applyFilter(int modeId) {
        EnumFilterMode mode = EnumFilterMode.getByValue(modeId);
        filterByType(mode.mode());
        View main = findViewById(R.id.constraintLayout);
        ColorDrawable[] color = {new ColorDrawable(getColor(currentMode.color())), new ColorDrawable(getColor(mode.color()))};
        currentMode = mode;
        TransitionDrawable trans = new TransitionDrawable(color);
        main.setBackground(trans);
        trans.startTransition(500);
    }

    /**
     * Method to handle list generation
     */
    private void generateListClick(){
        Intent intent = new Intent(this, ListDisplayActivity.class);
        String list = MovieListUtils.generateCurrentListString(currentMode.mode());
        intent.putExtra(EXTRA_MESSAGE, list);
        startActivity(intent);
    }

}
