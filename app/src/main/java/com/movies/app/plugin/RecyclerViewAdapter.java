package com.movies.app.plugin;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.movies.app.R;
import com.movies.app.utils.FileManager;
import com.movies.app.utils.Movie;
import com.movies.app.utils.MovieListUtils;
import com.movies.app.utils.SaveManager;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>  {
    private static ArrayList<Movie> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    RecyclerViewAdapter(Context context, ArrayList<Movie> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = ((ArrayList<Movie>) data.clone());
        setClickListener((MainActivity) context);
    }

    // data is passed into the constructor
    // inflates the row layout from xml when needed
    // binds the data to the TextView in each row
    // total number of rows
    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener, View.OnLongClickListener, CompoundButton.OnCheckedChangeListener{
        TextView textView;
        CheckBox checkBox;

        ViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.textView);
            checkBox = itemView.findViewById(R.id.checkBox);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
            checkBox.setOnCheckedChangeListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null){
                mClickListener.onItemClick(view, getAdapterPosition());
            }
        }

        @Override
        public boolean onLongClick(View view) {
            if (mClickListener != null) {
                return mClickListener.onItemLongClick(view, getAdapterPosition());
            }
            return false;
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked){
            // Update movie present value
            boolean newPresentVal = !mData.get(getAdapterPosition()).getPresent();
            mData.get(getAdapterPosition()).setPresent(newPresentVal);
            SaveManager.updateMoviesStatus(mData.get(getAdapterPosition()));
            FileManager.updateSaveFile(mInflater.getContext(), SaveManager.getDataList());
            mClickListener.onItemCheckChange(buttonView, getAdapterPosition());
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recyclerview_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String elem = getItemTitle(position);
        holder.textView.setText(elem);

        // Disable checkbox listener to avoid save file update at app starting
        holder.checkBox.setOnCheckedChangeListener(null);
        if(MovieListUtils.getByTitle(mData, holder.textView.getText().toString()) != null) {
            holder.checkBox.setChecked(MovieListUtils.getByTitle(mData,
                    holder.textView.getText().toString()).getPresent());
        }
        holder.checkBox.setOnCheckedChangeListener(holder);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public Movie getItem(int id){
        return mData.get(id);
    }

    // convenience method for getting data at click position
    String getItemTitle(int id) {
        return mData.get(id).getTitle();
    }

    public static ArrayList<Movie> getListMovie(){
        return mData;
    }

    // Add elem to recyclerview list
    public void addItem(Movie data){
        mData.add(data);
        notifyItemInserted(getItemCount()-1);
    }

    // Remove elem at index 'position'
    public void removeItem(int position) {
        notifyItemRemoved(position);

        mData.remove(position);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
        boolean onItemLongClick(View view, int position);
        void onItemCheckChange(View view, int position);
    }

    public void clear() {
        final int size = mData.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
               removeItem(0);
            }
        }
    }
}
