package com.movies.app.plugin;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.movies.app.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListDisplayActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_display);

        // Get the Intent that started this activity and extract the string
        Intent intent = getIntent();
        final List<String> message = new ArrayList<>(Arrays.asList(intent.getStringExtra(MainActivity.EXTRA_MESSAGE).split("\n")));
        ListView view = findViewById(R.id.list);

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_list_item_1, message);
        view.setAdapter(arrayAdapter);

//        listView.(message);
//        // Capture the layout's TextView and set the string as its text
//        TextView textView = findViewById(R.id.textView);
//        textView.setText(message);
    }
}
